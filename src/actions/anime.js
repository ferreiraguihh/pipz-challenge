import axios from 'axios';


export const findAllAnime = ({text, offset = 0, limit = 20}) => {
    return async (dispach) => {


        const response = await axios.get('https://kitsu.io/api/edge/anime', {
            params: {
                'page[limit]': limit,
                'page[offset]': offset,
                'filter[text]': text !== '' ? text : null,
            }
        });

        const result = response.data;
        dispach({
            type: 'LIST',
            data: result.data,
            count: result.meta.count,
        })
    }
}


export const viewAnime = (currentAnime) => {
    return (dispach) => {
        dispach({
            type: 'VIEW',
            currentAnime,
        })
    }
}



