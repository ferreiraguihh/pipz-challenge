import React, {Fragment} from 'react';

export default function Toggle({show, children}) {
    return show ? (<Fragment>{children}</Fragment>) : <Fragment/>
}
