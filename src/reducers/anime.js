const initialState = {
    count: 0,
    data: [],
    currentAnime: {
        attributes: {coverImage: {tiny: {}}},
    },
}

const reducerAnime = (state = initialState, action) => {

    if (action.type === 'LIST') {
        return {
            ...state,
            data: action.data,
            count: action.count,
        }
    }

    if (action.type === 'VIEW') {
        return {
            ...state,
            currentAnime: action.currentAnime,
        }
    }


    return state;
}


export default reducerAnime;





