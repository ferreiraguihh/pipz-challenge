import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';

const styles = {
    root: {
        marginTop: 10,
        marginBottom: 20,
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: '50%',
        justifyContent: 'end',
    },

    input: {
        marginLeft: 8,
        flex: 1,
    },
    iconButton: {
        padding: 10,
    },
    divider: {
        width: 1,
        height: 28,
        margin: 4,
    },
};

function Search(props) {
    const {classes, onChange} = props;

    return (
        <Paper className={classes.root} elevation={1}>
            <InputBase className={classes.input} placeholder="Search Animes" onChange={onChange} position="fixed"/>
            <IconButton className={classes.iconButton} aria-label="Search">
                <SearchIcon/>
            </IconButton>
        </Paper>
    );
}

Search.propTypes = {
    classes: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired,
};

export default withStyles(styles)(Search);
