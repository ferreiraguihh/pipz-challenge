import React from 'react';
import PropTypes from 'prop-types';

import Typography from '@material-ui/core/Typography';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import '../../css/App.css';

import toRenderProps from 'recompose/toRenderProps';
import withState from 'recompose/withState';

import {Link} from "react-router-dom";

const styles = theme => ({
    root: {
        width: '100%',
        backgroundImage: 'url(https://boxbreak.city/wp-content/uploads/Header-Background.png)',
        backgroundSize: 'contain',
        height: 110,
    },
    title: {
        height: '70%',
        display: 'flex',
        color: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: `"Open Sans", sans-serif`,
        fontWeight: 'bold'
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },

});

const WithState = toRenderProps(withState('anchorEl', 'updateAnchorEl', null));


function Header(props) {
    const {classes} = props;
    return (
        <WithState>
            {({anchorEl, updateAnchorEl}) => {
                const open = Boolean(anchorEl);
                const handleClose = () => {
                    updateAnchorEl(null);
                };
                return (
                    <Link to="/">
                        <Grid className={classes.root}>
                            <Typography className={classes.title} color="inherit" variant="h4">
                                Anime Now
                            </Typography>
                        </Grid>
                    </Link>
                );
            }}
        </WithState>
    );

}

Header.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Header);
