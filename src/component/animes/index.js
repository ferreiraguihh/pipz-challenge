import React, {Fragment} from 'react'
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import ReactPaginate from 'react-paginate';
import {withStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CircularProgress from "@material-ui/core/CircularProgress";
import Grid from '@material-ui/core/Grid';
import Toggle from "../../common/Toogle";
import Search from '../search';
import styles from './styles';
import * as actions from '../../actions/anime';


class Animes extends React.Component {

    state = {
        expanded: false,
        limit: 20,
        isLoading: true,
        isFirstTime: true,
        text: null,
    };

    constructor(props) {
        super(props);
        props.findAllAnime({})
    }


    componentWillReceiveProps() {
        this.setState({isLoading: false, isFirstTime: false});
    }


    handleSearch = (e) => {

        const text = e.target.value;

        const {limit} = this.state;
        const {findAllAnime} = this.props;

        clearTimeout(this.state.timeout);

        const timeout = setTimeout(() => {
            findAllAnime({offset: 0, limit, text});
        }, 1000);
        this.setState({timeout, isLoading: true, text});
    }

    handleClick = (anime) => {
        const {viewAnime, history} = this.props;

        viewAnime(anime);
        history.push({
            pathname: '/view',
        })
    };

    handlePageClick = data => {
        const {limit, text} = this.state;
        let selected = data.selected;
        let offset = Math.ceil(selected * limit);

        this.setState({offset, isLoading: true}, () => {
            this.props.findAllAnime({offset, limit, text});
        });
    };

    render() {
        const {classes, data, count} = this.props;
        const {isLoading, isFirstTime, limit} = this.state;

        return (
            <Fragment>
                <Search onChange={this.handleSearch}/>
                <Grid className={classes.container}>
                    <Toggle show={(!isFirstTime && data.length > 0)}>
                        <ReactPaginate
                            previousLabel={'Previous'}
                            nextLabel={'Next'}
                            breakLabel={'...'}
                            breakClassName={'break-me'}
                            pageCount={(count / limit)}
                            marginPagesDisplayed={2}
                            pageRangeDisplayed={5}
                            onPageChange={this.handlePageClick}
                            containerClassName={'pagination'}
                            subContainerClassName={'pages pagination'}
                            activeClassName={'active'}/>
                    </Toggle>
                    <Grid className={classes.content}>

                        <Toggle show={isLoading}> <CircularProgress size={150} className={classes.loading}/> </Toggle>
                        <Toggle show={(data.length <= 0 && !isLoading)}>
                            <Grid><Typography color='error' variant="h5">Nenhum resultado encontrado.</Typography></Grid>
                        </Toggle>
                        <Toggle show={!isLoading}>
                            {data.map(anime => (
                                <Card className={classes.card} onClick={() => this.handleClick(anime)}>
                                    <CardHeader
                                        className={`${classes.header} truncate`}
                                        title={anime.attributes.canonicalTitle}

                                    />
                                    <CardMedia
                                        className={classes.media}
                                        image={anime.attributes.posterImage.small}
                                        title={anime.attributes.canonicalTitle}
                                    />
                                    <Grid container
                                          direction="row"
                                          justify="space-evenly"
                                          alignItems="center"
                                          style={{marginTop: 5}}
                                    >
                                        <Typography>
                                            {anime.attributes.startDate}
                                        </Typography>
                                        <Typography>
                                            {anime.attributes.endDate}
                                        </Typography>
                                    </Grid>

                                    <Grid container
                                          direction="row"
                                          justify="space-evenly"
                                          alignItems="center"
                                          style={{marginTop: 5, marginBottom: 5}}
                                    >
                                        <Typography>
                                            {anime.attributes.averageRating}
                                        </Typography>
                                        <Typography>
                                            {anime.attributes.status}
                                        </Typography>
                                    </Grid>

                                </Card>
                            ))}
                        </Toggle>


                    </Grid>
                    <Toggle show={(!isFirstTime && data.length > 0)}>
                        <ReactPaginate
                            previousLabel={'Previous'}
                            nextLabel={'Next'}
                            breakLabel={'...'}
                            breakClassName={'break-me'}
                            pageCount={count}
                            marginPagesDisplayed={2}
                            pageRangeDisplayed={5}
                            onPageChange={this.handlePageClick}
                            containerClassName={'pagination'}
                            subContainerClassName={'pages pagination'}
                            activeClassName={'active'}
                        />
                    </Toggle>
                </Grid>
            </Fragment>
        );
    }
}

Animes.propTypes = {
    classes: PropTypes.object.isRequired,
    data: PropTypes.arrayOf(PropTypes.object),
    count: PropTypes.number,
    findAllAnime: PropTypes.func,
    viewAnime: PropTypes.func,
};

export default connect(({Anime}) => ({
        data: Anime.data,
        count: Anime.count
    }),
    dispatch => bindActionCreators(actions, dispatch))
(withStyles(styles)(withRouter(Animes)));





