export default theme => ({
    content: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        flexWrap: 'wrap',
        minHeight: '70vh',
    },
    card: {
        width: 223,
        height: 370,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        cursor: 'pointer',
        margin: 10
    },
    media: {
        height: '100%',
        width: '90%',
        backgroundSize: 'contain',
    },
    actions: {
        display: 'flex',
    },

    header: {
        padding: 16,
        width: '90%',
        textAlign: 'center',
    },

    loading: {
        marginTop: 150,
        color: '#2b8688',
    }

});

