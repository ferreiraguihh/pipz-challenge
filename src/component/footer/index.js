import React, {Component} from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import '../../css/App.css';
import logo from '../../img/pipz-automation-logo.svg';

export default class Footer extends Component {
    render() {
        return (
            <Grid container className="App-footer">
                    <Grid container
                          justify="center"
                          alignItems="center">
                        <div align="center">
                                <img src={logo}/>
                                <Typography variant="h6" color="inherit" style={{ marginTop: 10 }}>
                                  Front End Challenge
                                </Typography>
                        </div>

                    </Grid>
            </Grid>
        );
    }
}

