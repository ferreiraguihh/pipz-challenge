export default theme => ({
    content: {},

    card: {
        maxWidth: '60%',
        backgroundColor: 'rgba(0, 0, 0, 0.85)',
        borderRadius: 20,
        marginTop: 20,
        marginBottom: 50,
    },
    rating: {
        marginBottom: 5,
        textAlign: 'justify',
        fontSize: 15,
        color: '#F4D03F'
    },

    title: {
        color: 'white',
        fontSize: 30,
        marginTop: 20,
        marginBottom: 20,
    },

    subTitle: {
        color: 'white',
        marginTop: 30,
    },

    text: {
        fontSize: 18,
        textAlign: 'justify',
        color: 'white',
        padding: '0 50px 0 50px',
        marginTop: 20,
        marginBottom: 30,

    }
});
