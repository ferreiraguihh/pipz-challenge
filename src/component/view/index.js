import React from 'react';
import {connect} from 'react-redux';
import Grid from '@material-ui/core/Grid';
import {withStyles} from '@material-ui/core/styles';

import Header from '../header'
import Footer from '../footer'
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import YouTube from 'react-youtube';
import Typography from '@material-ui/core/Typography';
import '../../css/App.css';
import styles from './styles';

class ViewAnime extends React.Component {

    onReady = (event) => {
        event.target.pauseVideo();
    };

    render() {
        const {classes, currentAnime} = this.props;

        const opts = {
            height: '390',
            width: '640',
            playerVars: {
                autoplay: 1
            }
        };
        return (
                <Grid container
                      className="App-container"
                      direction="column"
                      justify="center"
                      alignItems="center">

                    <Card className={classes.card}>
                        <CardContent>
                            <Grid container direction="column" justify="center" alignItems="center">

                                    <Typography gutterBottom className={classes.title}>
                                        {currentAnime.attributes.canonicalTitle}
                                    </Typography>

                                <Grid className={classes.content}>
                                    <Grid container
                                          direction="row"
                                          justify="space-evenly"
                                          alignItems="center">

                                        <Typography className={classes.rating}>
                                            {currentAnime.attributes.ageRatingGuide}
                                        </Typography>

                                        <Typography className={classes.rating}>
                                            {currentAnime.attributes.startDate}
                                            {' / '}
                                            {currentAnime.attributes.endDate}
                                        </Typography>
                                    </Grid>
                                    <YouTube
                                        videoId={currentAnime.attributes.youtubeVideoId}
                                        opts={opts}
                                        onReady={this._onReady}
                                    />
                                    <Grid container
                                          direction="row"
                                          justify="space-evenly"
                                          alignItems="center"

                                    >
                                        <Typography className={classes.rating}>
                                            Popularity Rank:{' '}
                                            {currentAnime.attributes.popularityRank}
                                        </Typography>
                                        <Typography className={classes.rating}>
                                            {currentAnime.attributes.status}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Typography variant="h5" className={classes.subTitle} c>
                                        Synopsis
                                </Typography>
                                    <Typography className={classes.text}>
                                        {currentAnime.attributes.synopsis}
                                    </Typography>
                            </Grid>
                        </CardContent>
                    </Card>
                </Grid>
        )
    }
}


export default connect(({Anime}) => ({
    currentAnime: Anime.currentAnime
}))(withStyles(styles)(ViewAnime));

