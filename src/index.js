import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {applyMiddleware, compose, createStore} from 'redux';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import thunk from 'redux-thunk';
import createBrowserHistory from 'history/createBrowserHistory'
import './css/index.css';
import './css/App.css';
import Animes from './component/animes';
import View from './component/view';
import rootReducer from './reducers';
import Grid from "@material-ui/core/Grid/Grid";
import Header from "./component/header";
import Footer from "./component/footer";
import * as serviceWorker from './serviceWorker';

const customHistory = createBrowserHistory();

const store = createStore(
    rootReducer,
    {},
    compose(
        applyMiddleware(thunk),
        window.devToolsExtension ? window.devToolsExtension() : f => f
    )
);

ReactDOM.render(
    <Provider store={store}>
        <Router history={customHistory}>
            <Grid>
                <Header/>
                <Grid container className="App-container">
                    <Route exact path="/" component={Animes}/>
                    <Route exact path="/view" component={View}/>
                </Grid>
                <Footer/>
            </Grid>
        </Router>

    </Provider>,
    document.getElementById('root'));


serviceWorker.unregister();
