

## ANIME NOW - Challenge


### Available Scripts

In the project directory, you can run:

### ` npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.


### `npm run build`

Builds the app for production to the `dist` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!




### Table of contents
[Project structure](#project-structure)

[Installation](#installation)

[Configuration](#configuration)

[Technologies used](#technologies-used)

### Project structure

````

src/
|- components/                                # All components
|    |- animes/ 
|       |- index.js __________________________ # List all animes
|       |- styles.js _________________________ # Css modules styles
|    |- view/ 
|       |- index.js __________________________ # Anime details
|       |- styles.js _________________________ # Css modules styles
|    |- header/ 
|       |- index.js __________________________ # Header application
|    |- footer/ 
|       |- index.js __________________________ # Footer application
|    |- search/ 
|       |- index.js __________________________ # Input component search
|- css/
|  |- App.css ________________________________ # App styles
|  |- index.css ______________________________ # Common styles
|- img/________________________________________# All images application
|- reducers/ 
|       |- anime.js __________________________ # Anime reducer
|       |- index.js __________________________ # Combine all reducers 
|- actions/
    |- index.js ______________________________ # Anime action for redux
|- common/ ___________________________________ # Common components 
|- index.js __________________________________ # Application entry 
      
````


### Installation

1- Clone the project

`git clone https://ferreiraguihh@bitbucket.org/ferreiraguihh/pipz-challenge.git`

2-`npm install` to install npm packages

3- start dev server using `npm start`.

3- build and bundling your resources for production `npm run build`.


#### Technologies used

* [Webpack 4](https://github.com/webpack/webpack) 
* [Babel 8](https://github.com/babel/babel) [ transforming JSX and ES6,ES7,ES8 ]
* [React](https://github.com/facebook/react)
* [Redux](https://redux.js.org/)
* [CSS modules](https://github.com/css-modules/css-modules) [ Isolated style based on each component ] 
* [React-router 4.0](https://reacttraining.com/react-router/) 

Create by Guilherme Ferreira dos Santos
